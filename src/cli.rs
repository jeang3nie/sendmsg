use clap::{Arg, Command, ValueHint};

pub fn cli() -> Command {
    Command::new("sendmsg")
        .about("Send a Misfin message on the command line")
        .author("Nathan Fisher")
        .version(env!("CARGO_PKG_VERSION"))
        .args([
            Arg::new("sender")
                .help("The Misfin address of the sender")
                .required(true)
                .value_hint(ValueHint::EmailAddress)
                .num_args(1),
            Arg::new("receiver")
                .help(
                    "A comma separated list of Misfin addresses which will \
                    receive this message",
                )
                .required(true)
                .value_hint(ValueHint::EmailAddress)
                .value_delimiter(',')
                .num_args(1..),
            Arg::new("certificate")
                .help("A pem encoded certificate and key")
                .required(false)
                .short('c')
                .long("certificate")
                .value_hint(ValueHint::FilePath)
                .num_args(1),
            Arg::new("message")
                .help("The text of the message to be sent")
                .short('m')
                .long("message")
                .required(false)
                .num_args(1),
            Arg::new("file")
                .help("A text file containing the message to be sent")
                .short('f')
                .long("file")
                .conflicts_with("message")
                .required(false)
                .value_hint(ValueHint::FilePath)
                .num_args(1),
        ])
}
