use {
    clap::ArgMatches,
    dory::{
        prelude::{Certificate, ClientCertificateStore},
        Sender,
    },
    rustls_pemfile::{read_one, Item},
    std::{
        collections::HashMap,
        error::Error,
        fs::{self, File},
        io::BufReader,
        iter,
    },
};

mod cli;
mod edit;

fn get_cert(opts: &ArgMatches) -> Option<Certificate> {
    if let Some(cert) = opts.get_one::<String>("certificate") {
        let fd = File::open(cert).ok()?;
        let mut reader = BufReader::new(fd);
        let mut certificate = Certificate {
            der: vec![],
            key: vec![],
        };
        for item in iter::from_fn(|| read_one(&mut reader).transpose()) {
            match item {
                Ok(Item::X509Certificate(cert)) => {
                    if certificate.der.is_empty() {
                        certificate.der = cert;
                    } else {
                        return None;
                    }
                }
                Ok(Item::RSAKey(key)) => {
                    if certificate.key.is_empty() {
                        certificate.key = key;
                    } else {
                        return None;
                    }
                }
                Ok(Item::PKCS8Key(key)) => {
                    if certificate.key.is_empty() {
                        certificate.key = key;
                    } else {
                        return None;
                    }
                }
                Ok(Item::ECKey(key)) => {
                    if certificate.key.is_empty() {
                        certificate.key = key;
                    } else {
                        return None;
                    }
                }
                _ => {}
            }
        }
        if certificate.der.is_empty() && certificate.key.is_empty() {
            None
        } else {
            Some(certificate)
        }
    } else {
        None
    }
}

fn main() -> Result<(), Box<dyn Error>> {
    let opts = cli::cli().get_matches();
    let store = HashMap::<String, String>::new();
    let mut client_store = HashMap::<String, Certificate>::new();
    let Some(sender) = opts.get_one::<String>("sender") else {
        return Err("Missing sender".to_string().into());
    };
    if let Some(certificate) = get_cert(&opts) {
        let _old = client_store.insert_certificate(&sender.parse()?, certificate);
    }
    let msg = if let Some(m) = opts.get_one::<String>("message") {
        m.to_string()
    } else if let Some(file) = opts.get_one::<String>("file") {
        fs::read_to_string(file)?
    } else if let Some(m) = edit::new_message()? {
        m
    } else {
        return Ok(());
    };
    if let Some(recipient) = opts.get_many::<String>("receiver") {
        for r in recipient {
            let request_str = format!("misfin://{r} {msg}\r\n");
            let sndr = Sender::new(sender, &request_str, store.clone(), client_store.clone())?;
            let res = sndr.send();
            match res {
                Ok(resp) => println!("{}", resp.to_string()),
                Err(e) => eprintln!("{e}"),
            }
        }
    }
    Ok(())
}
