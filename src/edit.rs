use {
    std::{
        env,
        error::Error,
        fs::{self, File},
        process::Command,
    },
    tinyrand::{Rand, RandRange, Seeded, StdRand},
    tinyrand_std::clock_seed::ClockSeed,
};

pub(super) fn new_message() -> Result<Option<String>, Box<dyn Error>> {
    let seed = ClockSeed::default().next_u64();
    let mut rand = StdRand::seed(seed);
    let mut s = "/tmp/zond-".to_string();
    for _n in 0..9 {
        let c = char::from(rand.next_range(97_u32..123) as u8);
        s.push(c);
    }
    let fd = File::create(&s);
    drop(fd);
    edit(&s)?;
    let msg = fs::read_to_string(&s)?;
    fs::remove_file(&s)?;
    if !msg.is_empty() {
        Ok(Some(msg))
    } else {
        Ok(None)
    }
}

fn edit(file: &str) -> Result<(), Box<dyn Error>> {
    if let Ok(ed) = env::var("EDITOR") {
        run(&ed, file)
    } else {
        run("vi", file)
            .or_else(|_| run("vim", file))
            .or_else(|_| run("emacs", file))
            .or_else(|_| run("nano", file))
            .or_else(|_| run("ee", file))
    }?;
    Ok(())
}

fn run(handler: &str, arg: &str) -> Result<(), Box<dyn Error>> {
    Command::new(handler).arg(arg).status()?;
    Ok(())
}
